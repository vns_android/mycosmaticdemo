package com.virtuoso.cosmaticbag.wishlist;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.virtuoso.cosmaticbag.R;
import com.virtuoso.cosmaticbag.haircare.HairClean;


public class CustomWishMakeUpGridViewAdapter extends ArrayAdapter<WishMakeUp> {
	Context context;
	int layoutResourceId;
//	ArrayList<Item> data = new ArrayList<Item>();
	  ArrayList<WishMakeUp> data=new ArrayList<WishMakeUp>();

	public CustomWishMakeUpGridViewAdapter(Context context,int layoutResourceId,ArrayList<WishMakeUp> data) {
		super(context, layoutResourceId,data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
	}	

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View row = convertView;
		RecordHolder holder = null;

		if (row == null) {
			LayoutInflater inflater = ((Activity) context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);

			holder = new RecordHolder();
			holder.txtTitle = (TextView) row.findViewById(R.id.item_text);
			holder.imageItem = (ImageView) row.findViewById(R.id.item_image);
			row.setTag(holder);
		} else {
			holder = (RecordHolder) row.getTag();
		}

		//Item item = data.get(position);
		
		WishMakeUp picture = data.get(position);
		    //    holder.txtTitle.setText(picture._name);
		        //convert byte to bitmap take from contact class
		        
		        byte[] outImage=picture._image;
		        ByteArrayInputStream imageStream = new ByteArrayInputStream(outImage);
		        Bitmap theImage = BitmapFactory.decodeStream(imageStream);
		holder.txtTitle.setText(picture.getName());
		holder.imageItem.setImageBitmap(theImage);
		holder.imageItem.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) 
            {
            	WishMakeUp picture = data.get(position);
            	 byte[] outImage=picture._image;
            	 
            	 String img_name=picture.getName();       
            	 
         		Intent i = new Intent(context,com.virtuoso.cosmaticbag.utility.ImagePopUp.class);
					i.putExtra("image_byte", outImage);
					i.putExtra("image_name", img_name);
            		//context.finish();
					context.startActivity(i);
				//	overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );	           	
            	
            }
        });
		return row;

	}

	static class RecordHolder {
		TextView txtTitle;
		ImageView imageItem;

	}

}
