package com.virtuoso.cosmaticbag.wishlist;

import java.util.Random;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.virtuoso.cosmaticbag.R;


public class WishBodyCareProductDetails extends Activity
{

	Button save;
	EditText title;
	String titleName;
	byte imageInByte[];
	WishBodyCareDatabaseHandler db;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.products_detail);
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			imageInByte = extras.getByteArray("image_byte");			
		}
		/**
		 * create DatabaseHandler object
		 */
		db = new WishBodyCareDatabaseHandler(this);

		initUI();

	}

	private void initUI() 
	{
		save=(Button)findViewById(R.id.save);
		title =(EditText)findViewById(R.id.edit_title);	
		save.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0)
			{

				titleName = title.getText().toString();

				Random generator = new Random();
				int n = 10000;
				n = generator.nextInt(n);


				if(title.getText().toString().length() >0)
				{
					db.addContact(new WishBodyCare(titleName, imageInByte));
					Intent in = new Intent(WishBodyCareProductDetails.this,com.virtuoso.cosmaticbag.wishlist.WishBodyCareActivity.class);
					finish();
					startActivity(in);
					overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_right );
				}else
				{
					Toast.makeText(getApplicationContext(), "Please Enter Title/Name....!!",Toast.LENGTH_SHORT).show();
				}

			}
		});

	}
	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		
		Intent i_back = new Intent(WishBodyCareProductDetails.this,com.virtuoso.cosmaticbag.wishlist.WishBodyCareActivity.class);
		finish();		
		startActivity(i_back);		
		overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_right );
		
	}

}
