package com.virtuoso.cosmaticbag;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;

public class LookILikeActivity extends Activity implements OnClickListener
{
	ImageView look_makeup,look_haircare,look_celebrities,look_hottrends,look_classic,goback;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_look_like);
		initUI();
		
	}
	private void initUI()
	{
		look_makeup=(ImageView)findViewById(R.id.look_icon_makeup);
		look_haircare=(ImageView)findViewById(R.id.look_icon_hair);
		look_celebrities=(ImageView)findViewById(R.id.look_icon_celebrities);
		look_hottrends=(ImageView)findViewById(R.id.look_icon_hot_trends);
		look_classic=(ImageView)findViewById(R.id.look_icon_classics);
		goback=(ImageView)findViewById(R.id.goback);
		
		look_makeup.setOnClickListener(this);
		look_haircare.setOnClickListener(this);
		look_celebrities.setOnClickListener(this);
		look_hottrends.setOnClickListener(this);
		look_classic.setOnClickListener(this);
		goback.setOnClickListener(this);	
	}
	
	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
		case R.id.look_icon_makeup:
			
			Intent in_makeup = new Intent(LookILikeActivity.this,com.virtuoso.cosmaticbag.lookilike.LookMakeUpActivity.class);
			finish();
			startActivity(in_makeup);
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );		
			
			break;
		case R.id.look_icon_hair:
			Intent in_hair = new Intent(LookILikeActivity.this,com.virtuoso.cosmaticbag.lookilike.LookHairActivity.class);
			finish();
			startActivity(in_hair);
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );	
			
			break;
		case R.id.look_icon_celebrities:
			
			Intent in_celebrities = new Intent(LookILikeActivity.this,com.virtuoso.cosmaticbag.lookilike.LookCelebritiesActivity.class);
			finish();
			startActivity(in_celebrities);
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );	
			break;	
		case R.id.look_icon_hot_trends:
			
			Intent in_hot_trends = new Intent(LookILikeActivity.this,com.virtuoso.cosmaticbag.lookilike.LookHotTrendsActivity.class);
			finish();
			startActivity(in_hot_trends);
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );	
			break;	
		case R.id.look_icon_classics:
	
			Intent in_classics = new Intent(LookILikeActivity.this,com.virtuoso.cosmaticbag.lookilike.LookClassicsActivity.class);
			finish();
			startActivity(in_classics);
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );	
			break;	
		case R.id.goback:
			Intent in_back = new Intent(LookILikeActivity.this,com.virtuoso.cosmaticbag.HomeActivity.class);
			finish();
			startActivity(in_back);
			
			overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_right );	
			
			break;
		}		
		
		
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		
		Intent in = new Intent(LookILikeActivity.this,com.virtuoso.cosmaticbag.HomeActivity.class);
		finish();
		startActivity(in);
		
		overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_right );		
	}
}
