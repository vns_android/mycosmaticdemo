package com.virtuoso.cosmaticbag.bodycare;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ClipData.Item;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.virtuoso.cosmaticbag.R;
import com.virtuoso.cosmaticbag.utility.CropOption;
import com.virtuoso.cosmaticbag.utility.CropOptionAdapter;

public class BodyCleanserActivity extends Activity
{
	GridView gridView;
	ArrayList<Item> gridArray = new ArrayList<Item>();
	 CustomBodyCleanserGridViewAdapter customBodyCleanserGridAdapter;
	 ArrayList<BodyCleanser> imageArry = new ArrayList<BodyCleanser>();
		byte[] imageName;
		int imageId;
		Bitmap theImage;
		BodyCleanserDataBaseHandler db;
		Button addImage;
		private static final int PICK_FROM_CAMERA_IMAGE = 1;
		private static final int CAMERA_REQUEST = 1;
		private static final int PICK_FROM_GALLERY = 2;
		private static final int CROP_FROM_CAMERA = 3;
		private Uri mImageCaptureUri;
		String _path;
		ImageView back;
		

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.body_treatment_activity);
		
		gridView = (GridView) findViewById(R.id.gridView1);
		
		
		/**
		 * create DatabaseHandler object
		 */
		db = new BodyCleanserDataBaseHandler(this);
		/**
		 * Reading and getting all records from database
		 */
		List<BodyCleanser> contacts = db.getAllContacts();
		for (BodyCleanser cn : contacts) {
			String log = "ID:" + cn.getID() + " Name: " + cn.getName()
					+ " ,Image: " + cn.getImage();

			// Writing Contacts to log
			Log.d("Result: ", log);
			// add contacts data in arrayList
			imageArry.add(cn);

		}
		/**
		 * Set Data base Item into listview
		 */
		customBodyCleanserGridAdapter = new CustomBodyCleanserGridViewAdapter(this, R.layout.body_cleanser_row_grid,imageArry);
		gridView.setAdapter(customBodyCleanserGridAdapter);
		
		
		/**
		 * open dialog for choose camera/gallery
		 */

		final String[] option = new String[] { "Take from Camera",
				"Select from Gallery" };
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.select_dialog_item, option);
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setTitle("Select Option");
		builder.setAdapter(adapter, new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				Log.e("Selected Item", String.valueOf(which));
				if (which == 0) 
				{
					Intent intent 	 = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

					mImageCaptureUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(),
							"tmp_avatar_" + String.valueOf(System.currentTimeMillis()) + ".jpg"));


					intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri);

					try {
						intent.putExtra("return-data", true);

						startActivityForResult(intent, PICK_FROM_CAMERA_IMAGE);
					} catch (ActivityNotFoundException e) {
						e.printStackTrace();
					}
				
					
					
				//	callCamera();
				}
				if (which == 1) {
					callGallery();
				}

			}
		});
		final AlertDialog dialog = builder.create();

		addImage = (Button) findViewById(R.id.btnAdd);

		addImage.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				dialog.show();
			}
		});
		
		
		back=(ImageView)findViewById(R.id.goback);
		back.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v)
			{
				Intent in_back = new Intent(BodyCleanserActivity.this,com.virtuoso.cosmaticbag.BodyCareActivity.class);
				finish();
				startActivity(in_back);
				
				overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_right );
				
			}
		});
		
	}
	/**
	 * On activity result
	 */
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode != RESULT_OK)
			return;

		switch (requestCode)
		{
		
		case PICK_FROM_CAMERA_IMAGE:
			onPhotoTaken();
			doCrop();

			break;
		/*case PICK_FROM_CAMERA_IMAGE:

			Bundle extras = data.getExtras();

			if (extras != null) {
				Bitmap yourImage = extras.getParcelable("data");
				// convert bitmap to byte
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				yourImage.compress(Bitmap.CompressFormat.PNG, 100, stream);
				byte imageInByte[] = stream.toByteArray();
				Log.e("output before conversion", imageInByte.toString());
				// Inserting Contacts
				Log.d("Insert: ", "Inserting ..");
				
				  Random generator = new Random();
				    int n = 10000;
				    n = generator.nextInt(n);				    
			//	db.addContact(new Face("Face"+n, imageInByte));
			//	Intent i = new Intent(SQLiteDemoActivity.this,SQLiteDemoActivity.class);
				Intent i = new Intent(FaceActivity.this,com.virtuoso.cosmaticbag.haircare.FaceProductDetails.class);
				i.putExtra("image_byte", imageInByte);
				finish();
				startActivity(i);
				overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );		
				

			}
			break;*/
		case PICK_FROM_GALLERY:
			Bundle extras2 = data.getExtras();

			if (extras2 != null) {
				Bitmap yourImage = extras2.getParcelable("data");
				// convert bitmap to byte
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				yourImage.compress(Bitmap.CompressFormat.PNG, 100, stream);
				byte imageInByte[] = stream.toByteArray();
				Log.e("output before conversion", imageInByte.toString());
				// Inserting Contacts
				Log.d("Insert: ", "Inserting ..");
				
				/*  Random generator = new Random();
				    int n = 10000;
				    n = generator.nextInt(n);*/
				
			//	db.addContact(new Face("Face"+n, imageInByte));
			//	Intent i = new Intent(SQLiteDemoActivity.this,SQLiteDemoActivity.class);
				Intent ii = new Intent(BodyCleanserActivity.this,com.virtuoso.cosmaticbag.bodycare.BodyCleanserProductDetails.class);
				ii.putExtra("image_byte", imageInByte);
				finish();
				startActivity(ii);
				overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );		
				
			}

			break;
			
		case CROP_FROM_CAMERA:	    	
			Bundle extras1 = data.getExtras();

			if (extras1 != null) {	        	
				Bitmap yourImage = extras1.getParcelable("data");
				
				// convert bitmap to byte
				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				yourImage.compress(Bitmap.CompressFormat.PNG, 100, stream);
				byte imageInByte[] = stream.toByteArray();
				Log.e("output before conversion", imageInByte.toString());
				// Inserting Contacts
				Log.d("Insert: ", "Inserting ..");

//				db.addContact(new Face("Face"+n, imageInByte));
				//	Intent i = new Intent(SQLiteDemoActivity.this,SQLiteDemoActivity.class);
					Intent i = new Intent(BodyCleanserActivity.this,com.virtuoso.cosmaticbag.bodycare.BodyCleanserProductDetails.class);
					i.putExtra("image_byte", imageInByte);
					finish();
					startActivity(i);
					overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );	

			}

			try{
				File f1 = new File(mImageCaptureUri.getPath());            

				if (f1.exists()) f1.delete();
			}catch(Exception ee)
			{

			}

			break;
			
			
		}
	}

	
	protected void onPhotoTaken() {


		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 4;

		Bitmap bitmap = BitmapFactory.decodeFile(_path, options);		

	}
	/**
	 * open camera method
	 */
	public void callCamera() {
		/*Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		cameraIntent.putExtra("crop", "true");
		cameraIntent.putExtra("aspectX", 0);
		cameraIntent.putExtra("aspectY", 0);
		cameraIntent.putExtra("outputX", 200);
		cameraIntent.putExtra("outputY", 150);
		cameraIntent.putExtra("return-data", true);
		startActivityForResult(cameraIntent, CAMERA_REQUEST);
		
			*/
		
		
		Intent intent 	 = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

		mImageCaptureUri = Uri.fromFile(new File(Environment.getExternalStorageDirectory(),
				"tmp_avatar_" + String.valueOf(System.currentTimeMillis()) + ".jpg"));


		intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, mImageCaptureUri);

		try {
			intent.putExtra("crop", "true");
			intent.putExtra("aspectX", 0);
			intent.putExtra("aspectY", 0);
			intent.putExtra("outputX", 200);
			intent.putExtra("outputY", 150);
			intent.putExtra("return-data", true);

			startActivityForResult(intent, PICK_FROM_CAMERA_IMAGE);
		} catch (ActivityNotFoundException e) {
			e.printStackTrace();
		}	

	}

	/**
	 * open gallery method
	 */

	public void callGallery() {
		Intent intent = new Intent();
		intent.setType("image/*");
		intent.setAction(Intent.ACTION_GET_CONTENT);
		intent.putExtra("crop", "true");
		intent.putExtra("aspectX", 0);
		intent.putExtra("aspectY", 0);
		intent.putExtra("outputX", 200);
		intent.putExtra("outputY", 150);
		intent.putExtra("return-data", true);
		startActivityForResult(
				Intent.createChooser(intent, "Complete action using"),
				PICK_FROM_GALLERY);

	}
	
	private void doCrop() {

		final ArrayList<CropOption> cropOptions = new ArrayList<CropOption>();	



		//intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
		//	intent.putExtra("crop", "true");

		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setType("image/*");

		List<ResolveInfo> list = getPackageManager().queryIntentActivities( intent, 0 );

		int size = list.size();

		if (size == 0) {	        
			Toast.makeText(this, "Can not find image crop app", Toast.LENGTH_SHORT).show();

			return;
		} else {
			intent.setData(mImageCaptureUri);
			intent.putExtra("crop", "true");
			intent.putExtra("outputX", 200);
			intent.putExtra("outputY", 150);
			intent.putExtra("aspectX", 1);
			intent.putExtra("aspectY", 1);

			intent.putExtra("scale", true);
			intent.putExtra("return-data", true);

			if (size == 1) {
				Intent i 		= new Intent(intent);
				ResolveInfo res	= list.get(0);

				i.setComponent( new ComponentName(res.activityInfo.packageName, res.activityInfo.name));

				startActivityForResult(i, CROP_FROM_CAMERA);
			} else {
				for (ResolveInfo res : list) {
					final CropOption co = new CropOption();

					co.title 	= getPackageManager().getApplicationLabel(res.activityInfo.applicationInfo);
					co.icon		= getPackageManager().getApplicationIcon(res.activityInfo.applicationInfo);
					co.appIntent= new Intent(intent);

					co.appIntent.setComponent( new ComponentName(res.activityInfo.packageName, res.activityInfo.name));

					cropOptions.add(co);
				}

				CropOptionAdapter adapter = new CropOptionAdapter(getApplicationContext(), cropOptions);

				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle("Choose Crop App");
				builder.setAdapter( adapter, new DialogInterface.OnClickListener() {
					public void onClick( DialogInterface dialog, int item ) {
						startActivityForResult( cropOptions.get(item).appIntent, CROP_FROM_CAMERA);
					}
				});

				builder.setOnCancelListener( new DialogInterface.OnCancelListener() {
					@Override
					public void onCancel( DialogInterface dialog ) {

						if (mImageCaptureUri != null ) {
							getContentResolver().delete(mImageCaptureUri, null, null );
							mImageCaptureUri = null;
						}
					}
				} );

				AlertDialog alert = builder.create();

				alert.show();
			}
		}
	}

	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		
		Intent in = new Intent(BodyCleanserActivity.this,com.virtuoso.cosmaticbag.BodyCareActivity.class);
		finish();
		startActivity(in);
		
		overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_right );
		
	}

}
