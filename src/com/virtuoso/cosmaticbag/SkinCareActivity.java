package com.virtuoso.cosmaticbag;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;

public class SkinCareActivity extends Activity implements OnClickListener
{
	ImageView skin_day,skin_night,skin_treatment,goback;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_skin_care);
		
		initUI();
		
	}

	private void initUI() 
	{
		skin_day=(ImageView)findViewById(R.id.skin_icon_day);
		skin_night=(ImageView)findViewById(R.id.skin_icon_night);
		skin_treatment=(ImageView)findViewById(R.id.skin_icon_treatment);
		goback=(ImageView)findViewById(R.id.goback);
		
		skin_day.setOnClickListener(this);
		skin_night.setOnClickListener(this);
		skin_treatment.setOnClickListener(this);
		goback.setOnClickListener(this);	
	}

	@Override
	public void onClick(View v)
	{
		switch(v.getId())
		{
		case R.id.skin_icon_day:
			
			Intent in_day = new Intent(SkinCareActivity.this,com.virtuoso.cosmaticbag.skincare.DayActivity.class);
			finish();
			startActivity(in_day);
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );		
			
			break;
		case R.id.skin_icon_night:
			Intent in_night = new Intent(SkinCareActivity.this,com.virtuoso.cosmaticbag.skincare.NightActivity.class);
			finish();
			startActivity(in_night);
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );	
			
			break;
		case R.id.skin_icon_treatment:
			
			Intent in_treatment = new Intent(SkinCareActivity.this,com.virtuoso.cosmaticbag.skincare.SkinTreatmentActivity.class);
			finish();
			startActivity(in_treatment);
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );	
			break;
			
		case R.id.goback:
			Intent in_back = new Intent(SkinCareActivity.this,com.virtuoso.cosmaticbag.HomeActivity.class);
			finish();
			startActivity(in_back);			
			overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_right );
			
			break;
		}
		
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		
		Intent in = new Intent(SkinCareActivity.this,com.virtuoso.cosmaticbag.HomeActivity.class);
		finish();
		startActivity(in);		
		overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_right );
		
	}

}
