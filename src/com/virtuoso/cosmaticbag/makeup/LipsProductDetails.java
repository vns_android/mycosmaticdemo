package com.virtuoso.cosmaticbag.makeup;

import java.util.Random;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.virtuoso.cosmaticbag.R;

public class LipsProductDetails extends Activity
{
	
	Button save;
	EditText title;
	String titleName;
	byte imageInByte[];
	LipsDataBaseHandler db;
	/*Spinner spinner1,spinner2,spinner3,spinner4;
	
	 static final String[] Months1 = new String[] { "Sunday", "Monday",
	      "Tuesday", "April", "May", "June", "July", "August", "September",
	      "October", "November", "December" };
	 
	 static final String[] Months2 = new String[] { "January", "February",
	      "March", "April", "May", "June", "July", "August", "September",
	      "October", "November", "December" };
	 
	 static final String[] Months3 = new String[] { "Apple", "Orange",
	      "Mango", "April", "May", "June", "July", "August", "September",
	      "October", "November", "December" };
	 
	 static final String[] Months4 = new String[] { "Yess", "Hurrah",
	      "Sad", "April", "May", "June", "July", "August", "September",
	      "October", "November", "December" };*/

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.products_detail);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			imageInByte = extras.getByteArray("image_byte");			
		}
		/**
		 * create DatabaseHandler object
		 */
		db = new LipsDataBaseHandler(this);
		
		initUI();
		
	}

	private void initUI() 
	{
		save=(Button)findViewById(R.id.save);
		title =(EditText)findViewById(R.id.edit_title);	
		save.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) 
			{
				titleName = title.getText().toString();

				Random generator = new Random();
				int n = 10000;
				n = generator.nextInt(n);


				if(title.getText().toString().length() >0)
				{
					db.addContact(new Lips(titleName, imageInByte));	
				Intent in = new Intent(LipsProductDetails.this,com.virtuoso.cosmaticbag.makeup.LipsActivity.class);
				finish();
				startActivity(in);
				overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_right );
				}else
				{
					Toast.makeText(getApplicationContext(), "Please Enter Title/Name....!!",Toast.LENGTH_SHORT).show();
				}
			}
		});
		
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		
		Intent i_back = new Intent(LipsProductDetails.this,com.virtuoso.cosmaticbag.makeup.LipsActivity.class);
		finish();		
		startActivity(i_back);		
		overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_right );
		
	}

}
