package com.virtuoso.cosmaticbag.makeup;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.virtuoso.cosmaticbag.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

public class FaceProductDetails extends Activity
{
	
	Button save;
	byte imageInByte[];
	FaceDataBaseHandler db;
	EditText title;
	String titleName;
	/*Spinner spinner1,spinner2,spinner3,spinner4;
	
	 static final String[] Months1 = new String[] { "Sunday", "Monday",
	      "Tuesday", "April", "May", "June", "July", "August", "September",
	      "October", "November", "December" };
	 
	 static final String[] Months2 = new String[] { "January", "February",
	      "March", "April", "May", "June", "July", "August", "September",
	      "October", "November", "December" };
	 
	 static final String[] Months3 = new String[] { "Apple", "Orange",
	      "Mango", "April", "May", "June", "July", "August", "September",
	      "October", "November", "December" };
	 
	 static final String[] Months4 = new String[] { "Yess", "Hurrah",
	      "Sad", "April", "May", "June", "July", "August", "September",
	      "October", "November", "December" };*/

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.products_detail);
		
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			imageInByte = extras.getByteArray("image_byte");			
		}
		/**
		 * create DatabaseHandler object
		 */
		db = new FaceDataBaseHandler(this);
		
		initUI();
		
	}

	private void initUI() 
	{
		save=(Button)findViewById(R.id.save);
		title =(EditText)findViewById(R.id.edit_title);		
		save.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) 
			{
				
				titleName = title.getText().toString();
				
				 Random generator = new Random();
				    int n = 10000;
				    n = generator.nextInt(n);
				    
				    
				    if(title.getText().toString().length() >0)
				    {
				    	 db.addContact(new Face(titleName, imageInByte));
							
							Intent in = new Intent(FaceProductDetails.this,com.virtuoso.cosmaticbag.makeup.FaceActivity.class);
							finish();
							startActivity(in);
							overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_right );
				    }else
				    {
				    	Toast.makeText(getApplicationContext(), "Please Enter Title/Name....!!",Toast.LENGTH_SHORT).show();
				    }	   
				
			}
		});
		
	}
	
	public void addListenerOnSpinnerItemSelection() {
		/*spinner1 = (Spinner) findViewById(R.id.spinner_kind);
		spinner2 = (Spinner) findViewById(R.id.spinner_category);
		spinner3 = (Spinner) findViewById(R.id.spinner_season);
		spinner4 = (Spinner) findViewById(R.id.spinner_style);
		spinner1.setOnItemSelectedListener(new CustomOnItemSelectedListener());
		
		  // Spinner Drop down elements
        List<String> categories = new ArrayList<String>();
        categories.add("Automobile");
        categories.add("Business Services");
        categories.add("Computers");
        categories.add("Education");
        categories.add("Personal");
        categories.add("Travel");
 
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories); 
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); 
        // attaching data adapter to spinner
        spinner2.setAdapter(dataAdapter);
        
        
		spinner2.setOnItemSelectedListener(new CustomOnItemSelectedListener2());
		spinner3.setOnItemSelectedListener(new CustomOnItemSelectedListener3());
		spinner4.setOnItemSelectedListener(new CustomOnItemSelectedListener4());*/
	}

	public class CustomOnItemSelectedListener implements OnItemSelectedListener {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int pos,long id)
		{
			
			
		}
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub

		}	
	}	
	
	public class CustomOnItemSelectedListener2 implements OnItemSelectedListener {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int pos,long id)
		{
			
			
		}
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub

		}	
	}	
	public class CustomOnItemSelectedListener3 implements OnItemSelectedListener {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int pos,long id)
		{
			
			
		}
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub

		}	
	}	public class CustomOnItemSelectedListener4 implements OnItemSelectedListener {

		@Override
		public void onItemSelected(AdapterView<?> parent, View view, int pos,long id)
		{
			
			
		}
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub

		}	
	}	
	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		
		Intent i_back = new Intent(FaceProductDetails.this,com.virtuoso.cosmaticbag.makeup.FaceActivity.class);
		finish();		
		startActivity(i_back);		
		overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_right );
		
	}
	

}
