package com.virtuoso.cosmaticbag.utility;

import java.io.ByteArrayInputStream;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.RectF;
import android.os.Bundle;
import android.util.FloatMath;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.imagezoom.ImageAttacher;
import com.imagezoom.ImageAttacher.OnMatrixChangedListener;
import com.imagezoom.ImageAttacher.OnPhotoTapListener;
import com.virtuoso.cosmaticbag.R;

public class ImagePopUp extends Activity
{

	String titleName;
	byte imageInByte[];
	ImageView img,back;
	TextView title;
	
	
	 Matrix matrix = new Matrix();
	 Matrix savedMatrix = new Matrix();
	 PointF startPoint = new PointF();
	 PointF midPoint = new PointF();
	 float oldDist = 1f;
	 static final int NONE = 0;
	 static final int DRAG = 1;
	 static final int ZOOM = 2;
	 int mode = NONE;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.image_pop_up);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			imageInByte = extras.getByteArray("image_byte");
			titleName=extras.getString("image_name");
		}
		
		 img = (ImageView)findViewById(R.id.your_image);
		 back = (ImageView)findViewById(R.id.goback);
		 title=(TextView)findViewById(R.id.popup_image_name);
		 
		   ByteArrayInputStream imageStream = new ByteArrayInputStream(imageInByte);
	        Bitmap theImage = BitmapFactory.decodeStream(imageStream);
	        title.setText(titleName);
	        img.setImageBitmap(theImage);
	        
	        img.setOnClickListener(new View.OnClickListener() 
	        {
	        	@Override
				public void onClick(View arg0)
	        	{
					finish();					
				}
			});  
	        
	        
	        /**
	         * set on touch listner on image
	         */
	        img.setOnTouchListener(new View.OnTouchListener() {

	         @Override
	         public boolean onTouch(View v, MotionEvent event) {

	          ImageView view = (ImageView) v;
	          System.out.println("matrix=" + savedMatrix.toString());
	          switch (event.getAction() & MotionEvent.ACTION_MASK) {
	          case MotionEvent.ACTION_DOWN:

	           savedMatrix.set(matrix);
	           startPoint.set(event.getX(), event.getY());
	           mode = DRAG;
	           break;

	          case MotionEvent.ACTION_POINTER_DOWN:

	           oldDist = spacing(event);

	           if (oldDist > 10f) {
	            savedMatrix.set(matrix);
	            midPoint(midPoint, event);
	            mode = ZOOM;
	           }
	           break;

	          case MotionEvent.ACTION_UP:

	          case MotionEvent.ACTION_POINTER_UP:
	           mode = NONE;

	           break;

	          case MotionEvent.ACTION_MOVE:
	           if (mode == DRAG) {
	            matrix.set(savedMatrix);
	            matrix.postTranslate(event.getX() - startPoint.x,
	              event.getY() - startPoint.y);
	           } else if (mode == ZOOM) {
	            float newDist = spacing(event);
	            if (newDist > 10f) {
	             matrix.set(savedMatrix);
	             float scale = newDist / oldDist;
	             matrix.postScale(scale, scale, midPoint.x, midPoint.y);
	            }
	           }
	           break;

	          }
	          view.setImageMatrix(matrix);

	          return true;
	         }

	         @SuppressLint("FloatMath")
	         private float spacing(MotionEvent event) {
	          float x = event.getX(0) - event.getX(1);
	          float y = event.getY(0) - event.getY(1);
	          return FloatMath.sqrt(x * x + y * y);
	         }

	         private void midPoint(PointF point, MotionEvent event) {
	          float x = event.getX(0) + event.getX(1);
	          float y = event.getY(0) + event.getY(1);
	          point.set(x / 2, y / 2);
	         }
	        });  

	        /**
	         * Use Simple ImageView
	         */
	      //  usingSimpleImage(img);
 
		
	}
	
	
	
	 public void usingSimpleImage(ImageView imageView) {
	        ImageAttacher mAttacher = new ImageAttacher(imageView);
	        ImageAttacher.MAX_ZOOM = 2.0f; // Double the current Size
	        ImageAttacher.MIN_ZOOM = 0.5f; // Half the current Size
	        MatrixChangeListener mMaListener = new MatrixChangeListener();
	        mAttacher.setOnMatrixChangeListener(mMaListener);
	        PhotoTapListener mPhotoTap = new PhotoTapListener();
	        mAttacher.setOnPhotoTapListener(mPhotoTap);
	    }

	    private class PhotoTapListener implements OnPhotoTapListener {

	        @Override
	        public void onPhotoTap(View view, float x, float y) {
	        }
	    }

	    private class MatrixChangeListener implements OnMatrixChangedListener {

	        @Override
	        public void onMatrixChanged(RectF rect) {

	        }
	    }
	
	
	
	@Override
	public void onBackPressed() {
	    super.onBackPressed();	  
		finish();
		overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_right );
		
	}
	

}
