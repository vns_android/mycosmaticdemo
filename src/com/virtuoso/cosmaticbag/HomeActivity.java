package com.virtuoso.cosmaticbag;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.LinearLayout;

public class HomeActivity extends Activity implements OnClickListener
{	
	LinearLayout hair,skin,makeup,body,wishlist,looklike;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);		
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_home);
		
		initUI();		
		
	}
	private void initUI()
	{		
		hair=(LinearLayout)findViewById(R.id.hair_layout);
		skin=(LinearLayout)findViewById(R.id.skin_layout);
		body=(LinearLayout)findViewById(R.id.body_layout);
		makeup=(LinearLayout)findViewById(R.id.makeup_layout);
		wishlist=(LinearLayout)findViewById(R.id.wish_layout);
		looklike=(LinearLayout)findViewById(R.id.look_layout);	
		
		hair.setOnClickListener(this);
		skin.setOnClickListener(this);
		body.setOnClickListener(this);
		makeup.setOnClickListener(this);
		wishlist.setOnClickListener(this);
		looklike.setOnClickListener(this);
				
	}
	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
		case R.id.hair_layout:
			
			Intent in_hair = new Intent(HomeActivity.this,HairCareActivity.class);
			finish();
			startActivity(in_hair);
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );		
			
			break;
			
		case R.id.skin_layout:
			
			Intent in_skin = new Intent(HomeActivity.this,SkinCareActivity.class);
			finish();
			startActivity(in_skin);
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );			
			
			break;
			
		case R.id.body_layout:		
			
			Intent in_body = new Intent(HomeActivity.this,BodyCareActivity.class);
			finish();
			startActivity(in_body);
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );	
			break;
			
		case R.id.makeup_layout:
			
			Intent in_makeup = new Intent(HomeActivity.this,MakeUpActivity.class);
			finish();
			startActivity(in_makeup);
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );	
			
			break;
		case R.id.wish_layout:	
			
			Intent in_wish = new Intent(HomeActivity.this,WishListActivity.class);
			finish();
			startActivity(in_wish);
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );	
			break;
			
		case R.id.look_layout:		
			
			Intent in_look = new Intent(HomeActivity.this,LookILikeActivity.class);
			finish();
			startActivity(in_look);
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );	
			break;			
		
		}		
	}	

}
