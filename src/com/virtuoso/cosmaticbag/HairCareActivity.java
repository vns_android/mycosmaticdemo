package com.virtuoso.cosmaticbag;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;

import com.virtuoso.cosmaticbag.R;

public class HairCareActivity extends Activity implements OnClickListener
{
	ImageView hair_clean,hair_colour,hair_treatment,goback;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_hair_care);
		
		initUI();
		
	}

	private void initUI() {
		
		hair_clean=(ImageView)findViewById(R.id.hair_icon_clean);
		hair_colour=(ImageView)findViewById(R.id.hair_icon_colour);
		hair_treatment=(ImageView)findViewById(R.id.hair_icon_treatment);
		goback=(ImageView)findViewById(R.id.goback);
		
		hair_clean.setOnClickListener(this);
		hair_colour.setOnClickListener(this);
		hair_treatment.setOnClickListener(this);
		goback.setOnClickListener(this);	
		
	}

	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
		case R.id.hair_icon_clean:
			
			Intent in_clean = new Intent(HairCareActivity.this,com.virtuoso.cosmaticbag.haircare.HairCleanActivity.class);
			finish();
			startActivity(in_clean);
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );		
			
			break;
		case R.id.hair_icon_colour:
			Intent in_colour = new Intent(HairCareActivity.this,com.virtuoso.cosmaticbag.haircare.HairColourActivity.class);
			finish();
			startActivity(in_colour);
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );	
			
			break;
		case R.id.hair_icon_treatment:
			
			Intent in_treat = new Intent(HairCareActivity.this,com.virtuoso.cosmaticbag.haircare.HairTreatmentActivity.class);
			finish();
			startActivity(in_treat);
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );	
			break;		
			
		case R.id.goback:
			Intent in_back = new Intent(HairCareActivity.this,com.virtuoso.cosmaticbag.HomeActivity.class);
			finish();
			startActivity(in_back);
			
			overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_right );
			break;
			
		}		
	}
	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		
		Intent in = new Intent(HairCareActivity.this,com.virtuoso.cosmaticbag.HomeActivity.class);
		finish();
		startActivity(in);
		
		overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_right );
		
	}
	

}
