package com.virtuoso.cosmaticbag;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;

public class BodyCareActivity extends Activity implements OnClickListener
{
	ImageView body_cleanser,body_lotion,body_scrub,body_treatment,body_mouth,goback;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_body_care);

		initUI();

	}

	private void initUI()
	{
		body_cleanser=(ImageView)findViewById(R.id.body_icon_cleanser);
		body_lotion=(ImageView)findViewById(R.id.body_icon_lotion);
		body_scrub=(ImageView)findViewById(R.id.body_icon_scrub);
		body_treatment=(ImageView)findViewById(R.id.body_icon_treatment);
		body_mouth=(ImageView)findViewById(R.id.body_icon_mouth);	
		goback=(ImageView)findViewById(R.id.goback);

		body_cleanser.setOnClickListener(this);
		body_lotion.setOnClickListener(this);
		body_scrub.setOnClickListener(this);
		body_treatment.setOnClickListener(this);
		body_mouth.setOnClickListener(this);	
		goback.setOnClickListener(this);	
		

	}

	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
		case R.id.body_icon_cleanser:

			Intent in_cleanser = new Intent(BodyCareActivity.this,com.virtuoso.cosmaticbag.bodycare.BodyCleanserActivity.class);
			finish();
			startActivity(in_cleanser);
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );		

			break;
		case R.id.body_icon_lotion:
			Intent in_lotion = new Intent(BodyCareActivity.this,com.virtuoso.cosmaticbag.bodycare.BodyLotionActivity.class);
			finish();
			startActivity(in_lotion);
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );	

			break;
		case R.id.body_icon_scrub:

			Intent in_scrub = new Intent(BodyCareActivity.this,com.virtuoso.cosmaticbag.bodycare.BodyScrubActivity.class);
			finish();
			startActivity(in_scrub);
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );	
			break;		
		case R.id.body_icon_treatment:

			Intent in_treatment = new Intent(BodyCareActivity.this,com.virtuoso.cosmaticbag.bodycare.BodyTreatmentActivity.class);
			finish();
			startActivity(in_treatment);
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );		

			break;
		case R.id.body_icon_mouth:
			Intent in_mouth = new Intent(BodyCareActivity.this,com.virtuoso.cosmaticbag.bodycare.BodyMouthActivity.class);
			finish();
			startActivity(in_mouth);
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );	

			break;

		case R.id.goback:
			
			Intent in_back = new Intent(BodyCareActivity.this,com.virtuoso.cosmaticbag.HomeActivity.class);
			finish();
			startActivity(in_back);
			
			overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_right );
			
			break;
			

		}	

	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		
		Intent in = new Intent(BodyCareActivity.this,com.virtuoso.cosmaticbag.HomeActivity.class);
		finish();
		startActivity(in);
		
		overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_right );
		
	}


}
