package com.virtuoso.cosmaticbag;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.ImageView;

public class MakeUpActivity extends Activity implements OnClickListener
{
	ImageView face,lips,eyes,nails,goback;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_make_up);
		
		initUI();
		
	}

	private void initUI() {
		
		face=(ImageView)findViewById(R.id.icon_face);
		lips=(ImageView)findViewById(R.id.icon_lips);
		eyes=(ImageView)findViewById(R.id.icon_eyes);
		nails=(ImageView)findViewById(R.id.icon_nails);	
		goback=(ImageView)findViewById(R.id.goback);
		
		face.setOnClickListener(this);
		lips.setOnClickListener(this);
		eyes.setOnClickListener(this);
		nails.setOnClickListener(this);
		goback.setOnClickListener(this);	
	}

	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
		case R.id.icon_face:
			
			Intent in_face = new Intent(MakeUpActivity.this,com.virtuoso.cosmaticbag.makeup.FaceActivity.class);
			finish();
			startActivity(in_face);
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );		
			
			break;
		case R.id.icon_lips:
			Intent in_lips = new Intent(MakeUpActivity.this,com.virtuoso.cosmaticbag.makeup.LipsActivity.class);
			finish();
			startActivity(in_lips);
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );	
			
			break;
		case R.id.icon_eyes:
			
			Intent in_eyes = new Intent(MakeUpActivity.this,com.virtuoso.cosmaticbag.makeup.EyesActivity.class);
			finish();
			startActivity(in_eyes);
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );	
			break;
		case R.id.icon_nails:
			
			Intent in_nails = new Intent(MakeUpActivity.this,com.virtuoso.cosmaticbag.makeup.NailsActivity.class);
			finish();
			startActivity(in_nails);
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );	
			break;
		case R.id.goback:
			
			Intent in_back = new Intent(MakeUpActivity.this,com.virtuoso.cosmaticbag.HomeActivity.class);
			finish();
			startActivity(in_back);
			
			overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_right );
			
			break;
		
		}		
	}
	
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		
		Intent in = new Intent(MakeUpActivity.this,com.virtuoso.cosmaticbag.HomeActivity.class);
		finish();
		startActivity(in);
		
		overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_right );
		
	}
	
}
