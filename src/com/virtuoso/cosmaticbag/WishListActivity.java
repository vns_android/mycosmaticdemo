package com.virtuoso.cosmaticbag;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ImageView;

public class WishListActivity extends Activity implements OnClickListener
{
	ImageView wish_makeup,wish_haircare,wish_skincare,wish_bodycare,wish_list_treatment,goback;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_wish_list);
		
		initUI();
		
	}
	private void initUI() 
	{
		
		wish_makeup=(ImageView)findViewById(R.id.wish_icon_makeup);
		wish_haircare=(ImageView)findViewById(R.id.wish_icon_hair);
		wish_skincare=(ImageView)findViewById(R.id.wish_icon_skin);
		wish_bodycare=(ImageView)findViewById(R.id.wish_icon_body);
		wish_list_treatment=(ImageView)findViewById(R.id.wish_icon_treatment);
		goback=(ImageView)findViewById(R.id.goback);
		
		wish_makeup.setOnClickListener(this);
		wish_haircare.setOnClickListener(this);
		wish_skincare.setOnClickListener(this);
		wish_bodycare.setOnClickListener(this);
		wish_list_treatment.setOnClickListener(this);
		goback.setOnClickListener(this);	
		
	}
	
	@Override
	public void onClick(View v)
	{
		switch(v.getId())
		{
		case R.id.wish_icon_makeup:
			
			Intent in_makeup = new Intent(WishListActivity.this,com.virtuoso.cosmaticbag.wishlist.WishMakeUpActivity.class);
			finish();
			startActivity(in_makeup);
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );		
			
			break;
		case R.id.wish_icon_hair:
			Intent in_hair = new Intent(WishListActivity.this,com.virtuoso.cosmaticbag.wishlist.WishHairCareActivity.class);
			finish();
			startActivity(in_hair);
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );	
			
			break;
		case R.id.wish_icon_skin:
			
			Intent in_skin = new Intent(WishListActivity.this,com.virtuoso.cosmaticbag.wishlist.WishSkinCareActivity.class);
			finish();
			startActivity(in_skin);
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );	
			break;	
		case R.id.wish_icon_body:
			
			Intent in_body = new Intent(WishListActivity.this,com.virtuoso.cosmaticbag.wishlist.WishBodyCareActivity.class);
			finish();
			startActivity(in_body);
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );	
			break;	
		case R.id.wish_icon_treatment:
	
			Intent in_treatment = new Intent(WishListActivity.this,com.virtuoso.cosmaticbag.wishlist.WishListTreatmentActivity.class);
			finish();
			startActivity(in_treatment);
			overridePendingTransition( R.anim.slide_in_left, R.anim.slide_out_left );	
			break;	
		case R.id.goback:
			Intent in = new Intent(WishListActivity.this,com.virtuoso.cosmaticbag.HomeActivity.class);
			finish();
			startActivity(in);			
			overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_right );
			
			break;
		}		
		
	
		
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		
		Intent in = new Intent(WishListActivity.this,com.virtuoso.cosmaticbag.HomeActivity.class);
		finish();
		startActivity(in);		
		overridePendingTransition( R.anim.slide_in_right, R.anim.slide_out_right );
		
	}

}
