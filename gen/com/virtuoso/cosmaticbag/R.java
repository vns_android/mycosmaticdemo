/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.virtuoso.cosmaticbag;

public final class R {
    public static final class anim {
        public static final int slide_in_left=0x7f040000;
        public static final int slide_in_right=0x7f040001;
        public static final int slide_out_left=0x7f040002;
        public static final int slide_out_right=0x7f040003;
    }
    public static final class attr {
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 
 Default screen margins, per the Android Design guidelines. 
 Default screen margins, per the Android Design guidelines. 
 Default screen margins, per the Android Design guidelines. 
 Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f050000;
        public static final int activity_vertical_margin=0x7f050001;
        public static final int text_size=0x7f050002;
    }
    public static final class drawable {
        public static final int app_icon=0x7f020000;
        public static final int attach=0x7f020001;
        public static final int bodycarebg=0x7f020002;
        public static final int eyes=0x7f020003;
        public static final int facebg=0x7f020004;
        public static final int goback=0x7f020005;
        public static final int hair1=0x7f020006;
        public static final int haircare=0x7f020007;
        public static final int ic_launcher=0x7f020008;
        public static final int icon1=0x7f020009;
        public static final int icon2=0x7f02000a;
        public static final int lips=0x7f02000b;
        public static final int mainbg=0x7f02000c;
        public static final int makeup=0x7f02000d;
        public static final int mask=0x7f02000e;
        public static final int nails=0x7f02000f;
        public static final int orange_rack=0x7f020010;
        public static final int rack=0x7f020011;
        public static final int rackbg=0x7f020012;
        public static final int rounded_corner=0x7f020013;
        public static final int rounded_rectangle=0x7f020014;
        public static final int s_bodycare=0x7f020015;
        public static final int s_eyes=0x7f020016;
        public static final int s_face=0x7f020017;
        public static final int s_hair=0x7f020018;
        public static final int s_haircare=0x7f020019;
        public static final int s_lips=0x7f02001a;
        public static final int s_makeup=0x7f02001b;
        public static final int s_nails=0x7f02001c;
        public static final int s_skincare=0x7f02001d;
        public static final int sample=0x7f02001e;
        public static final int skincare=0x7f02001f;
        public static final int splash=0x7f020020;
        public static final int top_bar=0x7f020021;
    }
    public static final class id {
        public static final int action_settings=0x7f0900b4;
        public static final int akshiText=0x7f090028;
        public static final int bodyIcon=0x7f090023;
        public static final int bodyIconRack=0x7f090022;
        public static final int bodyMouthIconRack=0x7f090015;
        public static final int bodyTreatmentIconRack=0x7f090011;
        public static final int body_cleanser_layout=0x7f090003;
        public static final int body_cleanser_layout1=0x7f09008f;
        public static final int body_cleanser_rel_layout=0x7f090004;
        public static final int body_icon_cleanser=0x7f090006;
        public static final int body_icon_lotion=0x7f09000a;
        public static final int body_icon_mouth=0x7f090016;
        public static final int body_icon_scrub=0x7f09000e;
        public static final int body_icon_treatment=0x7f090012;
        public static final int body_layout=0x7f090020;
        public static final int body_lotion_layout=0x7f090007;
        public static final int body_lotion_layout1=0x7f090095;
        public static final int body_lotion_rel_layout=0x7f090008;
        public static final int body_mouth_layout=0x7f090013;
        public static final int body_mouth_layout1=0x7f090096;
        public static final int body_mouth_rel_layout=0x7f090014;
        public static final int body_scrub_layout=0x7f09000b;
        public static final int body_scrub_layout1=0x7f090097;
        public static final int body_scrub_rel_layout=0x7f09000c;
        public static final int body_treatment_layout=0x7f09000f;
        public static final int body_treatment_layout1=0x7f090098;
        public static final int body_treatment_rel_layout=0x7f090010;
        public static final int btnAdd=0x7f090091;
        public static final int cleanserIconRack=0x7f090005;
        public static final int contact_name=0x7f090090;
        public static final int edit_title=0x7f0900ab;
        public static final int eyes_layout=0x7f090069;
        public static final int face_layout=0x7f090067;
        public static final int goback=0x7f090001;
        public static final int gridView1=0x7f090092;
        public static final int hairCleanIconRack=0x7f090035;
        public static final int hairColourIconRack=0x7f090039;
        public static final int hairIcon=0x7f09001a;
        public static final int hairIconRack=0x7f090019;
        public static final int hairTreatmentIconRack=0x7f09003d;
        public static final int hair_clean_layout=0x7f090033;
        public static final int hair_clean_layout1=0x7f09009d;
        public static final int hair_clean_rel_layout=0x7f090034;
        public static final int hair_colour_layout=0x7f090037;
        public static final int hair_colour_layout1=0x7f09009e;
        public static final int hair_colour_rel_layout=0x7f090038;
        public static final int hair_icon_clean=0x7f090036;
        public static final int hair_icon_colour=0x7f09003a;
        public static final int hair_icon_treatment=0x7f09003e;
        public static final int hair_layout=0x7f090017;
        public static final int hair_treatment_layout=0x7f09003b;
        public static final int hair_treatment_layout1=0x7f09009f;
        public static final int hair_treatment_rel_layout=0x7f09003c;
        public static final int haircare_main_layout=0x7f090032;
        public static final int home_body_layout=0x7f090021;
        public static final int home_hair_layout=0x7f090018;
        public static final int home_look_layout=0x7f09002e;
        public static final int home_makeup_layout=0x7f090025;
        public static final int home_skin_layout=0x7f09001c;
        public static final int home_wish_layout=0x7f09002a;
        public static final int icon_eyes=0x7f090062;
        public static final int icon_face=0x7f09005a;
        public static final int icon_lips=0x7f09005e;
        public static final int icon_nails=0x7f090066;
        public static final int image_dialog_root=0x7f0900a2;
        public static final int item_image=0x7f090093;
        public static final int item_text=0x7f090094;
        public static final int iv_icon=0x7f090099;
        public static final int layout1=0x7f090002;
        public static final int lips_layout=0x7f090068;
        public static final int lookCelebritiesIconRack=0x7f09004b;
        public static final int lookClassicsIconRack=0x7f090053;
        public static final int lookHairIconRack=0x7f090047;
        public static final int lookHotTrendsIconRack=0x7f09004f;
        public static final int lookIcon=0x7f090030;
        public static final int lookIconRack=0x7f09002f;
        public static final int lookMakeupIconRack=0x7f090043;
        public static final int look_celebrities_layout=0x7f090049;
        public static final int look_celebrities_layout1=0x7f0900a5;
        public static final int look_celebrities_rel_layout=0x7f09004a;
        public static final int look_classics_layout=0x7f090051;
        public static final int look_classics_layout1=0x7f0900a6;
        public static final int look_classics_rel_layout=0x7f090052;
        public static final int look_hair_care_layout1=0x7f0900a7;
        public static final int look_hair_layout=0x7f090045;
        public static final int look_hair_rel_layout=0x7f090046;
        public static final int look_hot_trends_layout=0x7f09004d;
        public static final int look_hot_trends_layout1=0x7f0900a8;
        public static final int look_hot_trends_rel_layout=0x7f09004e;
        public static final int look_icon_celebrities=0x7f09004c;
        public static final int look_icon_classics=0x7f090054;
        public static final int look_icon_hair=0x7f090048;
        public static final int look_icon_hot_trends=0x7f090050;
        public static final int look_icon_makeup=0x7f090044;
        public static final int look_layout=0x7f09002d;
        public static final int look_main_layout=0x7f090040;
        public static final int look_makeup_layout=0x7f090041;
        public static final int look_makeup_layout1=0x7f0900a9;
        public static final int look_makeup_rel_layout=0x7f090042;
        public static final int lotionIconRack=0x7f090009;
        public static final int makeupEyesIconRack=0x7f090061;
        public static final int makeupFaceIconRack=0x7f090059;
        public static final int makeupIcon=0x7f090027;
        public static final int makeupIconRack=0x7f090026;
        public static final int makeupLipsIconRack=0x7f09005d;
        public static final int makeupNailsIconRack=0x7f090065;
        public static final int makeup_eyes_layout=0x7f09005f;
        public static final int makeup_eyes_layout1=0x7f09009b;
        public static final int makeup_eyes_rel_layout=0x7f090060;
        public static final int makeup_face_layout=0x7f090057;
        public static final int makeup_face_layout1=0x7f09009c;
        public static final int makeup_face_rel_layout=0x7f090058;
        public static final int makeup_layout=0x7f090024;
        public static final int makeup_lips_layout=0x7f09005b;
        public static final int makeup_lips_layout1=0x7f0900a4;
        public static final int makeup_lips_rel_layout=0x7f09005c;
        public static final int makeup_main_layout=0x7f090056;
        public static final int makeup_nails_layout=0x7f090063;
        public static final int makeup_nails_layout1=0x7f0900a0;
        public static final int makeup_nails_rel_layout=0x7f090064;
        public static final int nails_layout=0x7f09006a;
        public static final int popup_image_name=0x7f0900a1;
        public static final int save=0x7f0900ac;
        public static final int scrubIconRack=0x7f09000d;
        public static final int skinDayIconRack=0x7f09006f;
        public static final int skinIcon=0x7f09001f;
        public static final int skinIconRack=0x7f09001d;
        public static final int skinNightIconRack=0x7f090073;
        public static final int skinTreatmentIconRack=0x7f090077;
        public static final int skin_day_layout=0x7f09006d;
        public static final int skin_day_layout1=0x7f0900aa;
        public static final int skin_day_rel_layout=0x7f09006e;
        public static final int skin_icon_day=0x7f090070;
        public static final int skin_icon_night=0x7f090074;
        public static final int skin_icon_treatment=0x7f090078;
        public static final int skin_layout=0x7f09001b;
        public static final int skin_night_layout=0x7f090071;
        public static final int skin_night_layout1=0x7f0900ad;
        public static final int skin_night_rel_layout=0x7f090072;
        public static final int skin_treatment_layout=0x7f090075;
        public static final int skin_treatment_layout1=0x7f0900ae;
        public static final int skin_treatment_rel_layout=0x7f090076;
        public static final int skincare_main_layout=0x7f09006c;
        public static final int table=0x7f09001e;
        public static final int title_body_layout=0x7f090000;
        public static final int title_hair_layout=0x7f090031;
        public static final int title_look_like_layout=0x7f09003f;
        public static final int title_make_up_layout=0x7f090055;
        public static final int title_skincare_layout=0x7f09006b;
        public static final int title_wish_list_layout=0x7f090079;
        public static final int tv_name=0x7f09009a;
        public static final int wishBodyIconRack=0x7f090089;
        public static final int wishHairIconRack=0x7f090081;
        public static final int wishIcon=0x7f09002c;
        public static final int wishIconRack=0x7f09002b;
        public static final int wishMakeupIconRack=0x7f09007d;
        public static final int wishSkinIconRack=0x7f090085;
        public static final int wishTreatmentIconRack=0x7f09008d;
        public static final int wish_body_care_layout1=0x7f0900af;
        public static final int wish_body_layout=0x7f090087;
        public static final int wish_body_rel_layout=0x7f090088;
        public static final int wish_hair_care_layout1=0x7f0900b0;
        public static final int wish_hair_layout=0x7f09007f;
        public static final int wish_hair_rel_layout=0x7f090080;
        public static final int wish_icon_body=0x7f09008a;
        public static final int wish_icon_hair=0x7f090082;
        public static final int wish_icon_makeup=0x7f09007e;
        public static final int wish_icon_skin=0x7f090086;
        public static final int wish_icon_treatment=0x7f09008e;
        public static final int wish_layout=0x7f090029;
        public static final int wish_makeup_layout=0x7f09007b;
        public static final int wish_makeup_layout1=0x7f0900b2;
        public static final int wish_makeup_rel_layout=0x7f09007c;
        public static final int wish_skin_care_layout1=0x7f0900b3;
        public static final int wish_skin_layout=0x7f090083;
        public static final int wish_skin_rel_layout=0x7f090084;
        public static final int wish_treatment_layout=0x7f09008b;
        public static final int wish_treatment_rel_layout=0x7f09008c;
        public static final int wishlist_main_layout=0x7f09007a;
        public static final int wishlist_treatment_layout1=0x7f0900b1;
        public static final int your_image=0x7f0900a3;
    }
    public static final class layout {
        public static final int activity_body_care=0x7f030000;
        public static final int activity_body_care1=0x7f030001;
        public static final int activity_body_care2=0x7f030002;
        public static final int activity_hair_care=0x7f030003;
        public static final int activity_hair_care2=0x7f030004;
        public static final int activity_home=0x7f030005;
        public static final int activity_home1=0x7f030006;
        public static final int activity_home2=0x7f030007;
        public static final int activity_look_like=0x7f030008;
        public static final int activity_look_like2=0x7f030009;
        public static final int activity_make_up=0x7f03000a;
        public static final int activity_make_up2=0x7f03000b;
        public static final int activity_skin_care=0x7f03000c;
        public static final int activity_skin_care1=0x7f03000d;
        public static final int activity_skin_care2=0x7f03000e;
        public static final int activity_splash=0x7f03000f;
        public static final int activity_wish_list=0x7f030010;
        public static final int activity_wish_list2=0x7f030011;
        public static final int body_care=0x7f030012;
        public static final int body_cleanser_activity=0x7f030013;
        public static final int body_cleanser_row_grid=0x7f030014;
        public static final int body_lotion_activity=0x7f030015;
        public static final int body_lotion_row_grid=0x7f030016;
        public static final int body_mouth_activity=0x7f030017;
        public static final int body_mouth_row_grid=0x7f030018;
        public static final int body_scrub_activity=0x7f030019;
        public static final int body_scrub_row_grid=0x7f03001a;
        public static final int body_treatment_activity=0x7f03001b;
        public static final int body_treatment_row_grid=0x7f03001c;
        public static final int crop_selector=0x7f03001d;
        public static final int eyes_activity=0x7f03001e;
        public static final int eyes_row_grid=0x7f03001f;
        public static final int face_activity=0x7f030020;
        public static final int face_row_grid=0x7f030021;
        public static final int hair_care=0x7f030022;
        public static final int hair_clean_activity=0x7f030023;
        public static final int hair_clean_row_grid=0x7f030024;
        public static final int hair_colour_activity=0x7f030025;
        public static final int hair_colour_row_grid=0x7f030026;
        public static final int hair_treatment=0x7f030027;
        public static final int hair_treatment_row_grid=0x7f030028;
        public static final int home=0x7f030029;
        public static final int image_pop_up=0x7f03002a;
        public static final int lips_activity=0x7f03002b;
        public static final int lips_row_grid=0x7f03002c;
        public static final int look_celebrities_activity=0x7f03002d;
        public static final int look_celebrities_row_grid=0x7f03002e;
        public static final int look_classics_activity=0x7f03002f;
        public static final int look_classics_row_grid=0x7f030030;
        public static final int look_hair_care_activity=0x7f030031;
        public static final int look_hair_row_grid=0x7f030032;
        public static final int look_hot_trends_activity=0x7f030033;
        public static final int look_hot_trends_row_grid=0x7f030034;
        public static final int look_like=0x7f030035;
        public static final int look_make_up_activity=0x7f030036;
        public static final int look_makeup_row_grid=0x7f030037;
        public static final int make_up=0x7f030038;
        public static final int nails_activity=0x7f030039;
        public static final int nails_row_grid=0x7f03003a;
        public static final int products_detail=0x7f03003b;
        public static final int skin_care=0x7f03003c;
        public static final int skin_day_activity=0x7f03003d;
        public static final int skin_day_row_grid=0x7f03003e;
        public static final int skin_night_activity=0x7f03003f;
        public static final int skin_night_row_grid=0x7f030040;
        public static final int skin_treatment=0x7f030041;
        public static final int skin_treatment_row_grid=0x7f030042;
        public static final int wish_body_care_activity=0x7f030043;
        public static final int wish_bodycare_row_grid=0x7f030044;
        public static final int wish_hair_care_activity=0x7f030045;
        public static final int wish_haircare_row_grid=0x7f030046;
        public static final int wish_list=0x7f030047;
        public static final int wish_list_treatment_activity=0x7f030048;
        public static final int wish_make_up_activity=0x7f030049;
        public static final int wish_makeup_row_grid=0x7f03004a;
        public static final int wish_skin_care_activity=0x7f03004b;
        public static final int wish_skincare_row_grid=0x7f03004c;
        public static final int wish_treatment_row_grid=0x7f03004d;
    }
    public static final class menu {
        public static final int splash=0x7f080000;
    }
    public static final class string {
        public static final int action_settings=0x7f060001;
        public static final int app_name=0x7f060000;
        public static final int description_title_text=0x7f06000c;
        public static final int hello_world=0x7f060002;
        public static final int text_body_care=0x7f060005;
        public static final int text_celebrities=0x7f060009;
        public static final int text_classics=0x7f06000b;
        public static final int text_hair_care=0x7f060003;
        public static final int text_hot_trends=0x7f06000a;
        public static final int text_looks_i_like=0x7f060008;
        public static final int text_makeup=0x7f060006;
        public static final int text_skin_care=0x7f060004;
        public static final int text_wish_list=0x7f060007;
        public static final int txt_clean_condition=0x7f060015;
        public static final int txt_cleanser=0x7f060019;
        public static final int txt_colour=0x7f060016;
        public static final int txt_day=0x7f060011;
        public static final int txt_eyes=0x7f06000d;
        public static final int txt_face=0x7f06000e;
        public static final int txt_lips=0x7f06000f;
        public static final int txt_lotion=0x7f060017;
        public static final int txt_mouth_teeth=0x7f06001a;
        public static final int txt_nails=0x7f060010;
        public static final int txt_night=0x7f060012;
        public static final int txt_scrub=0x7f060018;
        public static final int txt_special_treatment=0x7f060013;
        public static final int txt_suppliments=0x7f060014;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        

        Base application theme for API 11+. This theme completely replaces
        AppBaseTheme from res/values/styles.xml on API 11+ devices.
    
 API 11 theme customizations can go here. 

        Base application theme for API 14+. This theme completely replaces
        AppBaseTheme from BOTH res/values/styles.xml and
        res/values-v11/styles.xml on API 14+ devices.
    
 API 14 theme customizations can go here. 
         */
        public static final int AppBaseTheme=0x7f070000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f070001;
        public static final int myDialogTheme=0x7f070002;
    }
}
